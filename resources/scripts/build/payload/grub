#!/usr/bin/env bash

#   generate GRUB ELF files (coreboot payload) and configuration files
#
#	Copyright (C) 2014, 2015, 2020, 2021 Leah Rowe <info@minifree.org>
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

[ "x${DEBUG+set}" = 'xset' ] && set -v
set -u -e

# This is where GRUB is expected to be (outside of the grub-assemble, instead in main checkout)
source "resources/grub/modules.list"

printf "Creating GRUB payloads and configuration files\n"

if [ ! -d "grub/" ]; then
	./download grub
fi

if [ ! -f "grub/grub-mkstandalone" ]; then
	./build module grub
fi

[ ! -d "payload/" ] && mkdir -p payload/
[ ! -d "payload/grub" ] && mkdir -p payload/grub/

rm -f payload/grub/*

# Separate GRUB payload per keymap. This saves space in the ROM, otherwise
# a lot of space would be used if every keymap was stored in a single image

for keylayoutfile in resources/grub/keymap/*.gkb; do
	if [ ! -f "${keylayoutfile}" ]; then
		continue
	fi
	keymap="${keylayoutfile##resources/grub/keymap/}"
	keymap="${keymap%.gkb}"

	grub/grub-mkstandalone \
		--grub-mkimage="grub/grub-mkimage" \
		-O i386-coreboot \
		-o payload/grub/grub_${keymap}.elf \
		-d grub/grub-core/ \
		--fonts= --themes= --locales=  \
		--modules="${grub_modules}" \
		--install-modules="${grub_install_modules}" \
		/boot/grub/grub.cfg=resources/grub/config/grub_memdisk.cfg \
		/boot/grub/layouts/${keymap}.gkb=${keylayoutfile}


	if [ "${keymap}" = "usqwerty" ]; then	
		cp resources/grub/config/grub.cfg payload/grub/grub_usqwerty.cfg
	else
		sed "s/usqwerty/${keymap}/" < resources/grub/config/grub.cfg > payload/grub/grub_${keymap}.cfg
	fi

	sed "s/grubtest.cfg/grub.cfg/" < payload/grub/grub_${keymap}.cfg > payload/grub/grub_${keymap}_test.cfg

	printf "Generated: 'payload/grub/grub_%s.elf' and configs.'\n" "${keymap}"
done

printf "Done! Check payload/grub/ to see the files.\n\n"
